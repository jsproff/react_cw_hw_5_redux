import { connect } from 'react-redux';

// IMPORT YOUR REACT COMPONENT
import App from '../components/App';
// IMPORT YOUR ACTIONS
import demoActionCreator from '../actions/demoActionCreator';

const mapStateToProps = (state, ownProps) => {
  return {
      MainPageStuff: state.testReducer.data,
      TodoStuff: state.todoReducer.todo
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    demoActionCreator: () => {
      dispatch( demoActionCreator.doSomsng() );
    },
    doSomeSyncStuff: () => {
      // do some stuff
      dispatch({
        type: 'ACTION_TYPE',
        payload: ['some', 'data', 'sync']
      });
    },
    addTodoItem: (todo) => {
      dispatch({
        type: 'ADD_TODOITEM',
        todo: todo
      });
    },
    removeTodoItem: (todoItem) => {
      dispatch({
        type: 'REMOVE_TODOITEM',
        todoItem: todoItem
      });
    }
  };
};

const MyApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);



export default MyApp;
