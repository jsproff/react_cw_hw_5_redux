import React from 'react';

const TodoItem = ({text, remove, id}) => {
    const x = () => remove(id);
    return(
        <li>
            <input type="checkbox"/>
            {text}
            <button onClick={x}>X</button>
        </li>
    )
};

export default TodoItem;