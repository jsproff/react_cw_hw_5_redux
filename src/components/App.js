import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Route, Link, NavLink } from 'react-router-dom';

import store from '../store';

import Todo from './todo'
import TodoItem from './todoitem'


const ListComponent = (params) =>{
  return(
    <div>
      <h1>List of something</h1>
      <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
      </ul>

    </div>
  );
};

class App extends Component {

  addTodoItem = (ev) => {
    if (ev.key === 'Enter') {
      const { addTodoItem, TodoStuff } = this.props;
      const target = ev.target;
      const todo = TodoStuff;
      todo.curID += 1;

      todo.todos.push({
        id: todo.curID,
        name: target.value
      });
      addTodoItem(todo);
      target.value = '';
    }
  }

  removeTodoItem = (id) => {
    const { removeTodoItem, TodoStuff } = this.props;
    const todos = [];
    TodoStuff.todos.map(item => {
      if(item.id !== id) {
        todos.push(item);
      }
    });
    removeTodoItem(todos);
  }

  render() {
    let {demoActionCreator, doSomeSyncStuff, TodoStuff} = this.props;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
          <div>
            <Link to="/">Main</Link>
            <Link to="/list">List</Link>
          </div>
          <button onClick={demoActionCreator}>
            Dispatch Action
          </button>
          <button onClick={doSomeSyncStuff}>
            Sync Action
          </button>
        </header>

        <Switch>
          <Route path="/" exact render={()=>{
            return(<h1>My Main route</h1>);
          }}/>
          <Route path="/list" component={ListComponent}/>
        </Switch>

          <Todo addItem={this.addTodoItem}>
              {
                TodoStuff.todos.map((item, index)=> {
                  return <TodoItem key={index} id={item.id} text={item.name} remove={this.removeTodoItem}/>
                })
              }
          </Todo>

      </div>
    );
  }
}

export default App;
