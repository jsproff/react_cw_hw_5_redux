import React from 'react';

const Todo = ({children, addItem}) => {
    return(
        <div className="todo-wrapper">
            <input type="text" onKeyUp={addItem}/>
            <ul>
                {children}
            </ul>
        </div>
    )
};

export default Todo;