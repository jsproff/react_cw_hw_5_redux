import { combineReducers } from 'redux';


const initialState = {
  course: 'React.js',
  studentsCount: 15,
  data: [],
  todo: {
    curID: 4,
    todos: [
      {
        id: 0,
        name: 'завтрак'
      },
      {
        id: 1,
        name: 'перекус'
      },
      {
        id: 2,
        name: 'обед'
      },
      {
        id: 3,
        name: 'перекус'
      },
      {
        id: 4,
        name: 'ужин'
      }
    ]
  }
};

function testReducer(state = initialState, action){
  switch( action.type ){
    case 'ACTION_TYPE':
      console.log('action!', action.payload);
      return Object.assign({}, state, { data: action.payload });
      // return state;
    case 'CHANGE_COURSE':
      // return { ...state, course: action.payload };
      return Object.assign({}, state, { course: action.payload });

    case 'ADD_STUDENT':
      return Object.assign({}, state, { studentsCount: state.studentsCount + 1 });

    case 'REMOVE_STUDENT':
      return Object.assign({}, state, { studentsCount: state.studentsCount - 1 });

    default:
      return state;
  }
}

function todoReducer(state = initialState, action) {
  switch( action.type ) {
    case 'ADD_TODOITEM':
      // return Object.assign({}, state, { todos: action.todoItem });
      return { ...state, todo: {todos: action.todo.todos, curID: action.todo.curID } };
    case 'REMOVE_TODOITEM':
      debugger;
      return { ...state, todo: { todos: action.todoItem } };
    default:
      return state;
  }
}

const reducer = combineReducers({
  testReducer,
  todoReducer
});

export default reducer;
